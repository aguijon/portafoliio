import React, { Component } from 'react'

export default class About extends Component {
  render() {
    return (      
      <section id="about">
        <div className="row">
          <div className="three columns">
            <img className="profile-pic" src="images/profilepic.jpg" alt />
          </div>
          <div className="nine columns main-col">
            <h2>Sobre mí</h2>
            <p>Emprendedor, me destaco por tener un rápido aprendizaje de las tareas que debo realizar. Proactivo en un equipo de trabajo manteniendo buenas relaciones con el entorno, Capacidad de Investigar, diagnosticar, diseñar construir, evaluar, auditar y mantener sistemas informáticos aplicados en las áreas administrativas, técnicas y  científicas; asimilación y uso de la tecnología de información.
            </p>
            <div className="row">
              <div className="columns contact-details">
                <h2>Detalles de contacto</h2>
                <p className="address">
                  <span>Calle 4 No. 6b-15 B. Santander </span><br />
                  <span>
                    La union, Colombia
                  </span><br />
                  <span>(+57) 3042079361</span><br />
                  <span>fabian.robledolave@gmail.com</span>
                </p>
              </div>
             <div className="columns download">
                <p>
                  <a href="#" className="button"><i className="fa fa-download" />Download Resume</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
