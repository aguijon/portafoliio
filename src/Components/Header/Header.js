import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <header id="home">
        <nav id="nav-wrap">
          <a className="mobile-btn" href="#nav-wrap" title="Show navigation">Show navigation</a>
          <a className="mobile-btn" href="#" title="Hide navigation">Hide navigation</a>
          <ul id="nav" className="nav">
          <li className="current"><a className="smoothscroll" href="#home">Inicio</a></li>
            <li><a className="smoothscroll" href="#about">Sobre Mi</a></li>
	         <li><a className="smoothscroll" href="#resume">Resume</a></li>
            <li><a className="smoothscroll" href="#portfolio">Trabajos</a></li>
            <li><a className="smoothscroll" href="#awards"> Recoomcimientos </a></li>
            <li><a className="smoothscroll" href="#contact">Contactame</a></li>
          </ul> {/* end #nav */}
        </nav> {/* end #nav-wrap */}
        <div className="row banner">
          <div className="banner-text">
            <h1 className="responsive-headline"> Soy Fabián Andres Robledo Olave.</h1>
            <h3> CEO & <span>Desarrollador</span> de Software  <span>en akiox.com,</span> creado increíbles aplicaciones web y móviles con<span> Laravel, vue, React y otros frameworks</span>. 
            <a className="smoothscroll" href="#about"></a>  mas abajo aprendera  <a className="smoothscroll" href="#about">más sobre mí</a>.</h3>
            <hr />

            <ul className="social">
              <li><a rel="noopener noreferrer" target="_blank" href="https://twitter.com/vandaLbd"><i className="fa fa-twitter" /></a></li>
              <li><a rel="noopener noreferrer" target="_blank" href="https://www.linkedin.com/in/leonardo-badilla-daza-427464144/"><i className="fa fa-linkedin" /></a></li>
              <li><a rel="noopener noreferrer" target="_blank" href="https://github.com/vandal777"><i className="fa fa-github" /></a></li>
            </ul>
          </div>
        </div>
        <p className="scrolldown">
          <a className="smoothscroll" href="#about"><i className="icon-down-circle" /></a>
        </p>
      </header>
    );
  }
}